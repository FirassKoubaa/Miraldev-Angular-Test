import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ListComponent} from './list/list.component';
import {ListSystemsComponent} from './list-systems/list-systems.component';
import {ListCountriesComponent} from 'app/list-countries/list-countries.component';
import {ListDevicesComponent} from './list-devices/list-devices.component';

export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'listBrowsers', component: ListComponent },
  { path: 'listSystems', component: ListSystemsComponent },
  { path: 'listCountries', component: ListCountriesComponent },
  { path: 'listDevices', component: ListDevicesComponent },
  { path: '', component: HomeComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
