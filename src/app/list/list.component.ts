import {Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/map'
import {HttpService} from '../http.service';
import {TreatmentService} from '../treatment.service';
import * as $ from 'jquery';
import * as Highcharts from 'highcharts';
import {IMyDpOptions} from 'mydatepicker';
import {ConfigService} from '../config.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConfigService, HttpService, TreatmentService]
})

export class ListComponent implements OnInit {

  // DatePicker config
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    showClearDateBtn : false,
    editableDateField: false
  };
  public model: Object = {date: {year: 2017, month: 8, day: 4}};
  // variables declaration
  public initialDate: string;
  public choosenDate: string;
  dataItems: any;
  nb = 0;
  browsers: { [key: string ]: number } = {};
  browserStats: any[] = [];
  private dataUrl = this.configService.dataUrl;  // URL to web api
  public chart: any;

  constructor(private dataServer: HttpService,
              private treatmentService: TreatmentService,
              private configService: ConfigService) {
  }

  // Datepicker onChange
  onChange(val) {
    // FILTER BROWSERS
    this.treatmentService.filterDimensions('g', this.dataItems, val.formatted.split('-').reverse().join('-'));
    // Publish stats and % in a table
    this.treatmentService.statsToArray(this.dataItems);
    // update Chart
    this.treatmentService.updateChart(this.chart);
  }

  ngOnInit() {
     this.initialDate = (new Date(this.model['date'].year, this.model['date'].month - 1, this.model['date'].day + 1))
      .toISOString().split('T')[0];
     this.dataServer.loadDataItems(this.dataUrl).subscribe(
      data => {
        this.dataItems = data;
        // FILTER BROWSERS
        this.treatmentService.filterDimensions('g', this.dataItems, this.initialDate);
        // Publish stats and % in a table
        this.treatmentService.statsToArray(this.dataItems);
        // HIGHCHART
        this.chart = Highcharts.chart('browsersChart', {
          chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
          },
          title: {
            text: 'Navigateurs / Browsers'
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: false
              },
              showInLegend: true
            }
          },
          series: [{
            name: 'Brands',
            colorByPoint: true,
            data: this.treatmentService.dimensionArray
          }]
        });
      }
    );
  }
}
