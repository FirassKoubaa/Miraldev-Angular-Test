import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  // URL to web api
    /* PHP backend */
    // public dataUrl = 'http://localhost/Miraldev/server.php';  // URL to web api

    /* Node.JS backend */
    public dataUrl = 'http://localhost:3000/data';
  constructor() { }
}
