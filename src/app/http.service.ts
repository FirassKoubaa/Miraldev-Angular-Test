import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
  constructor(private http: Http) {}

  loadDataItems(url: string) {
    return this.http.get(url)
      .map(data => {
        data.json();
        return data.json();
      });
  }
}
