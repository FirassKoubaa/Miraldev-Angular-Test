import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSystemsComponent } from './list-systems.component';

describe('ListSystemsComponent', () => {
  let component: ListSystemsComponent;
  let fixture: ComponentFixture<ListSystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
