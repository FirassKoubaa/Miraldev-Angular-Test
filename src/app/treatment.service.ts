import { Injectable } from '@angular/core';
import * as Highcharts from 'highcharts';

@Injectable()
export class TreatmentService {

  // variables declaration
  public initialDate: string;
  public choosenDate: string;
  dataItems: any;
  nb = 0;
  dimensionDictionary: { [key: string ]: number } = {};
  dimensionArray: any[] = [];
  constructor() { }

  public statsToArray(dataItems) {
    this.dimensionArray = [];
    for (let j in this.dimensionDictionary) {
      this.dimensionArray.push({name: j, y: (this.dimensionDictionary[j] / dataItems.length) * 100})
    }
  }

  public updateChart(chartVar) {
    chartVar.series[0].setData(this.dimensionArray);
    chartVar.redraw();
  }

  public filterDimensions(order, dataItems , date) {
    dataItems.forEach(item => {
      const wsDate = item.b.toString().split(' ')[0];
      this.choosenDate = date;
      if (wsDate === this.choosenDate) {
        if (this.dimensionDictionary[item[order]]) {
          this.dimensionDictionary[item[order]] = 1 + this.dimensionDictionary[item[order]];
        } else {
          this.dimensionDictionary[item[order]] = 1;
        }
      }
    });
  }
}
