import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import {AppRoutingModule} from './app-routing.module';
import { HttpModule } from '@angular/http';
import { MyDatePickerModule } from 'mydatepicker';
import {FormsModule} from "@angular/forms";
import { ListSystemsComponent } from './list-systems/list-systems.component';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { ListDevicesComponent } from './list-devices/list-devices.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListComponent,
    ListSystemsComponent,
    ListCountriesComponent,
    ListDevicesComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MyDatePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
