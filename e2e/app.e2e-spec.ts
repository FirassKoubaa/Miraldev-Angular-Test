import { MiraldevAngularTestPage } from './app.po';

describe('miraldev-angular-test App', () => {
  let page: MiraldevAngularTestPage;

  beforeEach(() => {
    page = new MiraldevAngularTestPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
